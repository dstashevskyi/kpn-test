import { defineConfig } from "vite";
import legacy from "@vitejs/plugin-legacy";
import vue2 from "@vitejs/plugin-vue2";
import { createSvgPlugin } from "vite-plugin-vue2-svg";

const path = require("path");

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue2(),
    legacy({
      targets: ["ie >= 11"],
      additionalLegacyPolyfills: ["regenerator-runtime/runtime"],
    }),
    createSvgPlugin(),
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./", "src"),
      "@assets": path.resolve(__dirname, "./", "src", "assets"),
      "@styles": path.resolve(__dirname, "./", "src", "assets", "styles"),
    },
  },
});
